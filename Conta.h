#pragma once
#include <iostream>

using namespace std;
class Conta
{
protected:
	float saldo;
	string numero;
	int tipo;
public:
	Conta();
	~Conta();
	void setSaldo(float saldo);
	void setNumero(string numero);
	float getSaldo();
	string getNumero();
	void deposita(float valor);
	void retira(float valor);
    virtual void imprimeExtrato();
	void aplicaJuros(int dias);
	int getTipo();
};

