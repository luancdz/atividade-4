// Atividade 04.cpp : Define o ponto de entrada para a aplica��o de console.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include "Conta.h"
#include "ContaCorrente.h"
#include "ContaPoupanca.h"
#include "ListaContas.h"

using namespace std;

int main()
{
	ListaContas* listaConta = new ListaContas();
		
	ContaCorrente* contaCorrenteAux = new ContaCorrente();
	contaCorrenteAux->setNumero("303060");
	contaCorrenteAux->setSaldo(11.00);
	listaConta->adiciona(contaCorrenteAux);	

	
	Conta *contaAux = new Conta();
	contaAux->setNumero("79797-96");
	contaAux->setSaldo(100.00);
	listaConta->adiciona(contaAux);

	ContaPoupanca* contaPoupancaAux = new ContaPoupanca();
	contaPoupancaAux->setNumero("508796");
	contaPoupancaAux->setSaldo(500.52);
	listaConta->adiciona(contaPoupancaAux);
	
	listaConta->imprime();

	listaConta->aplicaJuros(9);

	system("cls");

	listaConta->imprime();
	
	system("PAUSE");
	return 0;	
}


int menu() {
	cout << "Bem vindo ao banco HUEBR" << endl;
	cout << "Selecione o tipo de conta que deseja criar" << endl;
	cout << "1 - Conta corrente" << endl;
	cout << "2 - Conta poupanca" << endl;
	int contaTipo;
	do {
		
		cin >> contaTipo;
		system("cls");

	} while ((contaTipo != 1) || (contaTipo != 2));
	return contaTipo;
}

