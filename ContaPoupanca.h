#pragma once
#include "Conta.h"
class ContaPoupanca: virtual public Conta
{
private:
	int tipo;
public:
	ContaPoupanca();
	~ContaPoupanca();
	void imprimeExtrato();
	int getTipo();
	void aplicaJuros(int dias);
};

