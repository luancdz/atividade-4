#include "Conta.h"
#include "ContaCorrente.h"
#include "ContaPoupanca.h"

using namespace std;

	class ListaContas
{
private:
	Conta *conta[100];
	int tamanho;

public:
		ListaContas();
		~ListaContas();
		void adicionaContaCorrente(ContaCorrente *conta);
		void adiciona(Conta *conta);
		void adicionaContaPoupanca(ContaPoupanca *conta);
		void remove(string numero);
		void imprime();
		void aplicaJuros(int dias);
};