#include "Conta.h"

#pragma once
using namespace std;
class ContaCorrente: public Conta
{
private: 
	int tipo;

public:
	ContaCorrente();
	~ContaCorrente();
	void imprimeExtrato();
	int getTipo();
	void aplicaJuros(int dias);
};

